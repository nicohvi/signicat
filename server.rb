require 'sinatra'
require 'rest-client'
require 'savon'
# require 'base64'

NAME  = 'demo'.freeze
PW    = 'Bond007'.freeze

SDS_URL   = "https://#{NAME}:#{PW}@preprod.signicat.com/doc/demo/sds".freeze
DOC_URL   = "https://preprod.signicat.com/ws/documentservice-v3?wsdl".freeze

HEADERS = {
  'Content-Type' => 'application/pdf'
}

file = File.read("#{Dir.pwd}/file.pdf")

client = Savon.client(
  wsdl: DOC_URL, 
  namespaces: {
    'xmlns:doc' => 'https://id.signicat.com/definitions/wsdl/Document-v3'
  },
  namespace_identifier: :doc,
  env_namespace: :soapenv,
  log: true,
  pretty_print_xml: true  )

get '/' do
  res = RestClient.post(SDS_URL, file, HEADERS)
  refsid = res.to_s

  request = {
    subject: [{
      :@id => 'Brevio',
      'content!': {
        username: 'Brevio'
      }
    }],
    document: [
      {
        :@id => 'doc-1',
        '@xsi:type': 'doc:sds-document',
        '@ref-sds-id': refsid,
        'content!': {
          description: 'Brevio test',
          'send-to-archive': "false"
        }
      }
    ],
    task: [{
      :@id => 'Task1',
      'content!': {
        signature: {
          method: {
            'content!': 'nbid-sign'
          }
        },
        'document-action': [
        {
          :@type => 'sign',
          'content!': {
            'order-index': 0,
            optional: 'false',
            'document-ref': 'doc-1',
          }
        }],
      }
    }],
    'client-reference': 'brevio-test'
  }


  begin
    # ops = client.operation(:create_request)
    # pretty = ops.build(message: { request, }).pretty
    # p pretty
    soap_response = client.call(:create_request, message: { 
      request: [request], 
      service: 'demo', 
      password: 'Bond007',
      :attributes! => { request: { task: { id: 'Task1' } } }
    })
  rescue => e 
    p e.message
  end
  # soap_response.inspect
end